# Meal Planner Database Application!
* Dave Greenfield (dg2815)
* Justin Pugliese (jp3571)

### PostgreSQL Database
* schema = jp3571

### Application URL
http://23.96.24.226:8111/

### Description
We developed all the functionality we had planned to in Project 1.1.

### Highlights
The Explore tab shows some of the advanced queries we introduced in Project 1.2.

The user's profile page (`/profile`) has some good examples of selecting entities based on the current logged in user. To do this, we have to JOIN multiple tables together based on their relationship to the user.

The ingredients page (`/ingredients`) has some complex queries in that it enables creation of ingredients and the ability to relate the ingredient to a recipe in a single statement.


### Changes to Database Since Project 1.2
Sequences for autogenerating ID's were added to all tables where the user creates entities in the interface including customer,goal,ingredient,meal,meal_component,mealplan and restaurant.
The sequances all look for max existing ids to increment.

Check constraints were added to a number of fields, including a check for @ sign in email and positive prices suggested by the TA.

A trigger was added to Mealplan to add an end_date to an existing mealplan for a user, when a new mealplan was assigned.

Primary Key on customer_mealplan was changed to not null on customer_id/mealplan_id to allow a person to change back to a mealplan they had before.


### Demo Interactions

* Change the user context on the Home Tab
* View the User Profile of the New User from the User Dropdown
* View Mealplans and Restaurants
* Create A New Meal and Add a Dish to it
* Add a new goal to a user and view it in the user profile
* Remove the added goal in the user profile
* View the existing dishes on the dishes tab
* View Goals
* Try to add a goal to a mealplan that is already there (Atkins diet - General health)
* Create a new recipe and add an ingredient
* Try to add an ingredient with a negative quantity
* Assign a new goal to a User on Users Tab
* Assign a new mealplan in User.  Show in database how end date is added to old goal.
* Change back to original mealplan.  
* View Explore tab to see where users have mealplans that don't match their goals


