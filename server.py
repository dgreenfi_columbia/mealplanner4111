#!/usr/bin/env python2.7

"""
Columbia's COMS W4111.001 Introduction to Databases
Example Webserver

To run locally:

    python server.py

Go to http://localhost:8111 in your browser.

A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""

import os
import sys
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from flask import Flask, request, render_template, g, redirect, Response
import time
tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)

#default to first user in DB



#
# The following is a dummy URI that does not connect to a valid database. You will need to modify it to connect to your Part 2 database in order to use the data.
#
# XXX: The URI should be in the format of:
#
#     postgresql://USER:PASSWORD@w4111a.eastus.cloudapp.azure.com/proj1part2
#
# For example, if you had username gravano and password foobar, then the following line would be:
#
#     DATABASEURI = "postgresql://gravano:foobar@w4111a.eastus.cloudapp.azure.com/proj1part2"
#
DATABASEURI = "postgresql://jp3571:new4you1@w4111vm.eastus.cloudapp.azure.com/w4111"
CUSTOMER={'name':None,'customer_id':None}
SQLERROR = {'value':None}

#
# This line creates a database engine that knows how to connect to the URI above.
#
engine = create_engine(DATABASEURI)

#
# Example of running queries in your database
# Note that this will probably not work if you already have a table named 'test' in your database, containing meaningful data. This is only an example showing you how to run queries in your database using SQLAlchemy.
#
#engine.execute("""CREATE TABLE IF NOT EXISTS test (
#  id serial,
#  name text
#);""")
#engine.execute("""INSERT INTO test(name) VALUES ('grace hopper'), ('alan turing'), ('ada lovelace');""")

@app.before_first_request
def get_default_user():
    g.conn = engine.connect()
    cursor = g.conn.execute("SELECT customer_id,firstname,lastname FROM customer limit 1")
    for result in cursor:
      CUSTOMER['name']=result['firstname'] + " " + result['lastname']
      CUSTOMER['customer_id']=result['customer_id']
    cursor.close()


@app.before_request
def before_request():
  """
  This function is run at the beginning of every web request
  (every time you enter an address in the web browser).
  We use it to setup a database connection that can be used throughout the request.

  The variable g is globally accessible.
  """
  try:
    g.conn = engine.connect()

  except:
    print "uh oh, problem connecting to database"
    import traceback; traceback.print_exc()
    g.conn = None

@app.teardown_request
def teardown_request(exception):
  """
  At the end of the web request, this makes sure to close the database connection.
  If you don't, the database could run out of memory!
  """
  try:
    g.conn.close()
  except Exception as e:
    pass


#
# @app.route is a decorator around index() that means:
#   run index() whenever the user tries to access the "/" path using a GET request
#
# If you wanted the user to go to, for example, localhost:8111/foobar/ with POST or GET then you could use:
#
#       @app.route("/foobar/", methods=["POST", "GET"])
#
# PROTIP: (the trailing / in the path is important)
#
# see for routing: http://flask.pocoo.org/docs/0.10/quickstart/#routing
# see for decorators: http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
#
@app.route('/')
def index():
  """
  request is a special object that Flask provides to access web request information:

  request.method:   "GET" or "POST"
  request.form:     if the browser submitted a form, this contains the data in the form
  request.args:     dictionary of URL arguments, e.g., {a:1, b:2} for http://localhost?a=1&b=2

  See its API: http://flask.pocoo.org/docs/0.10/api/#incoming-request-data
  """

  # DEBUG: this is debugging code to see what request looks like
  print request.args


  #
  # example of a database query
  #
  cursor = g.conn.execute("SELECT customer_id,firstname,lastname FROM customer")
  names = []
  for result in cursor:
    cname=result['firstname']
    if result['lastname']!=None:
      cname+= " " + result['lastname']
    names.append({'name':cname,'customer_id':result['customer_id']})  # can also be accessed using result[0]
  cursor.close()

  #
  # Flask uses Jinja templates, which is an extension to HTML where you can
  # pass data to a template and dynamically generate HTML based on the data
  # (you can think of it as simple PHP)
  # documentation: https://realpython.com/blog/python/primer-on-jinja-templating/
  #
  # You can see an example template in templates/index.html
  #
  # context are the variables that are passed to the template.
  # for example, "data" key in the context variable defined below will be
  # accessible as a variable in index.html:
  #
  #     # will print: [u'grace hopper', u'alan turing', u'ada lovelace']
  #     <div>{{data}}</div>
  #
  #     # creates a <div> tag for each element in data
  #     # will print:
  #     #
  #     #   <div>grace hopper</div>
  #     #   <div>alan turing</div>
  #     #   <div>ada lovelace</div>
  #     #
  #     {% for n in data %}
  #     <div>{{n}}</div>
  #     {% endfor %}
  #
  context = dict(data = names)
  context['customer']=CUSTOMER
  print context


  #
  # render_template looks in the templates/ folder for files.
  # for example, the below file reads template/index.html
  #
  return render_template("index.html", **context)

#
# This is an example of a different path.  You can see it at:
#
#     localhost:8111/another
#
# Notice that the function name is another() rather than index()
# The functions for each app.route need to have different names
#

@app.route('/profile')
def profile():
  id=CUSTOMER['customer_id']
  #get native customer data
  cursor = g.conn.execute("SELECT customer_id,firstname,lastname,email FROM customer WHERE customer_id=%s",id)
  for result in cursor:
    cdata={}
    cdata['customer_id']=result['customer_id']
    cdata['name']=result['firstname']+ " " + str(result['lastname'])
    cdata['email']=result['email']
    cdata['goals']=[]
    cdata['mealplan']=""
  cursor.close()
  #get mealplan data
  cursor = g.conn.execute("SELECT m.name FROM customer c \
  JOIN customer_mealplan cm on c.customer_id=cm.customer_id \
  JOIN mealplan m on m.mealplan_id=cm.mealplan_id \
  WHERE c.customer_id=%s",id)
  for result in cursor:
    cdata['mealplan']=result['name']
  cursor.close()
  #get goal for user
  cursor = g.conn.execute("SELECT g.goal_id, g.name FROM customer c \
  JOIN goal_customer gc on c.customer_id=gc.customer_id \
  JOIN goal g on g.goal_id=gc.goal_id \
  WHERE c.customer_id=%s",id)
  for result in cursor:
    cdata['goals'].append({'name':result['name'], 'id':result['goal_id']})
  cursor.close()

  print cdata
  context = dict(data = cdata)
  context['customer']=CUSTOMER
  if SQLERROR['value'] != None:
    context['sqlerror'] = SQLERROR['value']
    SQLERROR['value'] = None
  return render_template("profile.html",**context)

@app.route('/meals')
def meals():
  print request.args
  cursor = g.conn.execute("SELECT name,description FROM meal")
  names = []
  descriptions=[]
  for result in cursor:
    names.append(result['name']+' - '+str(result['description']))
    # can also be accessed using result[0]
  cursor.close()
  context = dict(data = names)
  context['customer']=CUSTOMER
  if SQLERROR['value'] != None:
    context['sqlerror'] = SQLERROR['value']
    SQLERROR['value'] = None

  cursor = g.conn.execute("SELECT meal_comp_id,name FROM meal_component")
  mealcomponents = []
  for result in cursor:
    mealcomponents.append({'id':result['meal_comp_id'],'name':result['name']})

  context['mealcomponents']=mealcomponents
  cursor.close()
  cursor = g.conn.execute("SELECT meal_id,name FROM meal")
  meals = []
  for result in cursor:
    meals.append({'id':result['meal_id'],'name':result['name']})

  context['meals']=meals
  cursor.close()
  return render_template("meals.html",**context)

@app.route('/mealplans')
def mealplans():
  print request.args
  cursor = g.conn.execute("SELECT name,description FROM mealplan")
  names = []
  descriptions=[]
  for result in cursor:
    names.append(result['name']+' - '+str(result['description']))
    # can also be accessed using result[0]
  cursor.close()
  context = dict(data = names)
  context['customer']=CUSTOMER
  if SQLERROR['value'] != None:
    context['sqlerror'] = SQLERROR['value']
    SQLERROR['value'] = None
  return render_template("mealplans.html",**context)

@app.route('/restaurants')
def restaurants():
  cursor = g.conn.execute("SELECT name,address FROM restaurant")
  data = []
  for result in cursor:
    data.append(result['name']+' - '+str(result['address']))
    # can also be accessed using result[0]
  cursor.close()
  context = dict(data = data)
  context['customer']=CUSTOMER
  if SQLERROR['value'] != None:
    context['sqlerror'] = SQLERROR['value']
    SQLERROR['value'] = None
  return render_template("restaurants.html",**context)

@app.route('/users')
def users():
  cursor = g.conn.execute("SELECT firstname,lastname,email,customer_id FROM customer")
  data = []
  users=[]
  for result in cursor:
    data.append(str(result['firstname'])+' '+str(result['lastname'])+' - '+str(result['email']))
    users.append({'name':str(result['firstname'])+' '+str(result['lastname']),"id":result['customer_id']})
  cursor.close()
  context = dict(data = data)
  context['customer']=CUSTOMER
  context['users']=users
  if SQLERROR['value'] != None:
    context['sqlerror'] = SQLERROR['value']
    SQLERROR['value'] = None

  cursor = g.conn.execute("SELECT goal_id,name FROM goal")
  goals = []
  for result in cursor:
    goals.append({'id':result['goal_id'],'name':result['name']})

  context['goals']=goals

  cursor = g.conn.execute("SELECT M.mealplan_id,M.name AS mealplan_name,G.name AS goal_name \
    FROM mealplan M JOIN goal_mealplan GM \
    ON M.mealplan_id = GM.mealplan_id \
    JOIN goal G ON GM.goal_id = G.goal_id;")
  mealplans = []
  for result in cursor:
    mealplans.append({'id':result['mealplan_id'],'name':result['mealplan_name'], 'goal':result['goal_name']})

  context['mealplans']=mealplans
  cursor.close()
  return render_template("users.html",**context)

@app.route('/dishes')
def dishes():
  context={}

  cursor = g.conn.execute("SELECT mc.meal_comp_id,mc.name,mc.image_url from meal_component mc\
  JOIN home_dish hd ON hd.home_dish_id=mc.meal_comp_id")
  homedishes = []
  for result in cursor:
    homedishes.append({'id':result['meal_comp_id'],'name':result['name'],'image_url':result['image_url']})

  context['homedishes']=homedishes
  cursor.close()
  cursor = g.conn.execute("SELECT mc.meal_comp_id,mc.name,mc.image_url from meal_component mc \
  JOIN restaurant_dish rd ON rd.restaurant_dish_id=mc.meal_comp_id")
  restdishes = []
  for result in cursor:
    restdishes.append({'id':result['meal_comp_id'],'name':result['name'],'image_url':result['image_url']})
  context['restdishes']=restdishes
  cursor.close()
  context['customer']=CUSTOMER
  if SQLERROR['value'] != None:
    context['sqlerror'] = SQLERROR['value']
    SQLERROR['value'] = None
  return render_template("dishes.html",**context)

@app.route('/recipes')
def recipes():
  context={}

  cursor = g.conn.execute("SELECT ingredient_id,name FROM ingredient ORDER BY name")
  ingredients = []
  for result in cursor:
    ingredients.append({'id':result['ingredient_id'],'name':result['name']})

  context['ingredients']=ingredients
  cursor.close()
  cursor = g.conn.execute("SELECT recipe_id,name FROM recipe ORDER BY name ")
  recipes = []
  for result in cursor:
    recipes.append({'id':result['recipe_id'],'name':result['name']})
  context['recipes']=recipes
  cursor.close()
  context['customer']=CUSTOMER
  if SQLERROR['value'] != None:
    context['sqlerror'] = SQLERROR['value']
    SQLERROR['value'] = None
  return render_template("recipes.html",**context)

@app.route('/goals')
def goals():
  cursor = g.conn.execute("SELECT name FROM goal")
  data = []
  for result in cursor:
    data.append(result['name'])
    # can also be accessed using result[0]
  cursor.close()
  context = dict(data = data)
  context['customer']=CUSTOMER
  if SQLERROR['value'] != None:
    context['sqlerror'] = SQLERROR['value']
    SQLERROR['value'] = None
   ## meal data
  cursor = g.conn.execute("SELECT meal_id,name FROM meal")
  meals = []
  for result in cursor:
    meals.append({'id':result['meal_id'],'name':result['name']})

  context['meals']=meals
  cursor.close()

  ## goal data
  cursor = g.conn.execute("SELECT goal_id,name FROM goal")
  goals = []
  for result in cursor:
    goals.append({'id':result['goal_id'],'name':result['name']})

  context['goals']=goals
  cursor.close()
  ## mealplan data
  cursor = g.conn.execute("SELECT mealplan_id,name FROM mealplan")
  mealplans = []
  for result in cursor:
    mealplans.append({'id':result['mealplan_id'],'name':result['name']})

  context['mealplans']=mealplans
  cursor.close()

  return render_template("goals.html",**context)

@app.route('/explore')
def explore():
  #goals of all users and their mealplans
  print "explore"
  cursor = g.conn.execute("SELECT c.firstname,c.lastname,g.name AS custgoal,g2.name AS mealplangoal from customer c \
    JOIN goal_customer gc ON c.customer_id=gc.customer_id \
    JOIN goal g ON gc.goal_id=g.goal_id  \
    JOIN customer_mealplan m ON c.customer_id=m.customer_id \
    JOIN goal_mealplan gm ON m.mealplan_id=gm.mealplan_id  \
    JOIN goal g2 ON gm.goal_id=g2.goal_id \
    WHERE gc.end_date is NULL; ")

  cdata = {'names':[],'usergoals':[],'mealplangoals':[]}
  for result in cursor:
    cdata['names'].append(result['firstname']+ " " + str(result['lastname']))
    cdata['usergoals'].append(result['custgoal'])
    cdata['mealplangoals'].append(result['mealplangoal'])

  cursor.close()
  context = dict(data = cdata)
  context['customer']=CUSTOMER
  if SQLERROR['value'] != None:
    context['sqlerror'] = SQLERROR['value']
    SQLERROR['value'] = None

  cursor = g.conn.execute("SELECT mp.name,count(mp.mealplan_id) AS rest_mealnum from mealplan mp \
    JOIN meal_mealplan mm on mp.mealplan_id=mm.mealplan_id \
    JOIN meal m on m.meal_id=mm.meal_id \
    JOIN restaurant_dish rm on rm.restaurant_dish_id=m.meal_id \
    GROUP BY mp.name \
    HAVING count(mp.mealplan_id) >1;")
  context['restplans']=[]
  for result in cursor:
    context['restplans'].append({'planname':result['name'],'mealcount':result['rest_mealnum']})
  cursor.close()

  cursor = g.conn.execute("SELECT i.name,COUNT(i.name) from recipe r \
  join ingredient_recipe ir ON ir.recipe_id=r.recipe_id \
  join ingredient i ON i.ingredient_id=ir.ingredient_id \
  GROUP BY i.name \
  ORDER BY COUNT(i.name) desc;")
  context['topingr']=[]
  for result in cursor:
    context['topingr'].append({'name':result['name'],'count':result['count']})
  cursor.close()

  return render_template("explore.html",**context)



# Example of adding new data to the database
@app.route('/add', methods=['POST'])
def add():
  name = request.form['name']
  g.conn.execute('INSERT INTO test VALUES (NULL, ?)', name)
  return redirect('/')

@app.route('/addmeal', methods=['POST'])
def addmeal():
  try:
    name = request.form['name']
    description=request.form['description']
    g.conn.execute("INSERT INTO meal (name, description) VALUES (%s, %s)",(name, description))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/meals')

@app.route('/addmealplan', methods=['POST'])
def addmealplan():
  try:
    name = request.form['name']
    description=request.form['description']
    g.conn.execute("INSERT INTO mealplan (name, description) VALUES (%s, %s)",(name, description))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/mealplans')

@app.route('/addrestaurant', methods=['POST'])
def addrestaurant():
  try:
    name = request.form['name']
    website = request.form['website']
    address=request.form['address']
    g.conn.execute("INSERT INTO restaurant (name, website, address) VALUES (%s, %s, %s)",(name, website, address))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/restaurants')

@app.route('/adduser', methods=['POST'])
def adduser():
  try:
    fname = request.form['fname']
    lname = request.form['lname']
    email = request.form['email']
    g.conn.execute("INSERT INTO customer (firstname, lastname, email) VALUES (%s, %s, %s)",(fname, lname, email))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/users')

@app.route('/addgoal', methods=['POST'])
def addgoal():
  try:
    name = request.form['name']
    g.conn.execute("INSERT INTO goal (name) VALUES (%s)",(name))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/goals')

@app.route('/changeuser', methods=['POST'])
def changeuser():
  try:
    id=request.form['customer_id']
    cursor = g.conn.execute("SELECT customer_id,firstname,lastname FROM customer WHERE customer_id=%s",id)
    for result in cursor:
      CUSTOMER['name']=str(result['firstname'])
      last = str(result['lastname'])
      if last != 'None':
        CUSTOMER['name']=str(result['firstname']) + " " + last
      CUSTOMER['customer_id']=result['customer_id']
    cursor.close()
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/')

@app.route('/addusergoal', methods=['POST'])
def addusergoal():
  try:
    gid = request.form['goalid']
    uid = request.form['userid']
    begin_date=time.strftime("%Y-%m-%d")
    g.conn.execute("INSERT INTO goal_customer (goal_id, customer_id, begin_date,end_date) VALUES (%s, %s,%s,NULL)",(gid, uid,begin_date))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/users')

@app.route('/addusermealplan', methods=['POST'])
def addusermealplan():
  try:
    mpid = request.form['mealplanid']
    uid = request.form['userid']
    begin_date=time.strftime("%Y-%m-%d")
    g.conn.execute("INSERT INTO customer_mealplan (customer_id,mealplan_id, begin_date,end_date) VALUES (%s, %s,%s,NULL)",(uid, mpid,begin_date))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/users')


@app.route('/adddish', methods=['POST'])
def adddish():
  try:
    image_url=request.form['image_url']
    name = request.form['name']
    description=request.form['description']
    g.conn.execute("INSERT INTO meal_component (name, description,image_url) VALUES (%s, %s,%s)",(name, description,image_url))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/dishes')

@app.route('/addingr', methods=['POST'])
def addingr():
  try:
    name = request.form['name']
    g.conn.execute("INSERT INTO ingredient (name) VALUES (%s)",(name))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/recipes')

@app.route('/addrec', methods=['POST'])
def addrec():
  try:
    name = request.form['name']
    website = request.form['website']
    g.conn.execute("INSERT INTO recipe (name,website) VALUES (%s,%s)",(name,website))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/recipes')

@app.route('/addingr_recipe', methods=['POST'])
def addingr_recipe():
  try:
    ingredient_id = request.form['ingredient_id']
    recipe_id = request.form['recipe_id']
    quantity = request.form['quantity']
    unit = request.form['unit']
    g.conn.execute("INSERT INTO ingredient_recipe (ingredient_id,recipe_id,quantity,unit) VALUES (%s,%s,%s,%s)",(ingredient_id,recipe_id,quantity,unit))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/recipes')

@app.route('/addmeal_mealcom', methods=['POST'])
def addmeal_mealcom():
  try:
    meal_id = request.form['meal_id']
    meal_component_id = request.form['meal_component_id']
    g.conn.execute("INSERT INTO meal_meal_component (meal_id,meal_component_id) VALUES (%s,%s)",(meal_id,meal_component_id))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/meals')

@app.route('/addgoalmeal', methods=['POST'])
def addgoalmeal():
  try:
    goal_id = request.form['goal_id']
    meal_id = request.form['meal_id']
    g.conn.execute("INSERT INTO goal_meal (goal_id,meal_id) VALUES (%s,%s)",(goal_id,meal_id))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/goals')

@app.route('/addgoalmealplan', methods=['POST'])
def addgoalmealplan():
  try:
    goal_id = request.form['goal_id']
    mealplan_id = request.form['mealplan_id']
    g.conn.execute("INSERT INTO goal_mealplan (goal_id,mealplan_id) VALUES (%s,%s)",(goal_id,mealplan_id))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/goals')



@app.route('/removeusergoal', methods=['POST'])
def removeusergoal():
  try:
    id=CUSTOMER['customer_id']
    gid = request.form['goal_id']
    g.conn.execute("DELETE FROM goal_customer WHERE customer_id=%s \
      AND goal_id=%s", (id, gid))
  except:
    SQLERROR['value'] = sys.exc_info()
  return redirect('/profile')



@app.route('/login')
def login():
    #abort(401)
    #this_is_never_executed()
    pass

@app.errorhandler(Exception)
def exception_handler(error):
    return "!!!!"  + repr(error)

if __name__ == "__main__":
  import click

  @click.command()
  @click.option('--debug', is_flag=True)
  @click.option('--threaded', is_flag=True)
  @click.argument('HOST', default='0.0.0.0')
  @click.argument('PORT', default=8111, type=int)
  def run(debug, threaded, host, port):
    """
    This function handles command line parameters.
    Run the server using:

        python server.py

    Show the help text using:

        python server.py --help

    """

    HOST, PORT = host, port
    print "running on %s:%d" % (HOST, PORT)

    app.run(host=HOST, port=PORT, debug=True, threaded=threaded)


  run()
